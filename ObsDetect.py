#!/usr/bin/env python
import cv2					#access to the code in openCV 2 module
import cv					#access to the code in openCV module
import numpy as np			#access to the code in numpy module
import freenect				#access to the code in freenect module
import frame_convert		#access to the code in frame_convert module
import serial				#access to the code in serial module
import socket 

# reference ground is made
def ref_ground():
	for x in range(480):
		for y in range(640):
#			ref_gro[x][y]=(49.769*np.exp(-0.009*x))#+(0.2043*np.exp(-0.003*x))
			ref_gro[x][y]=(31.841*np.exp(-0.008*x))+(0.2043*np.exp(-0.003*x))
	ref_gro[ref_gro>=8]=8
	print ref_gro[300][1]
	print ref_gro[350][1]
	print ref_gro[430][1]
	print ref_gro[479][1]
	print 'reference ground is made'
	return ref_gro

#Grabs a depth map from the Kinect sensor
def getDepthKinect():
	(depth,_)=freenect.sync_get_depth()
	return depth
	
#Grabs a rgb frame from the Kinect sensor	
def getRGBKinect():
	rgb=frame_convert.video_cv(freenect.sync_get_video()[0])
	return rgb

#convert depth data to Meter
def convDepthToMeter(depth):
	DTM=1/((depth)*-0.0030711016 + 3.3309495161)
	
	return DTM
def remove_ground(DTM1):
	mask=DTM1.copy()
	mask[mask<=0]=0
	mask[mask>0]=1
	mask=mask.astype(np.uint8)
	DTM_obstacle=np.zeros((480,640))
	DTM_obstacle=cv2.subtract(ref_gro,DTM1,sp,mask)
	DTM_obstacle[DTM_obstacle<=.6]=0
	mask_obs=DTM_obstacle.copy()
	mask_obs[mask_obs>0]=1
	mask_obs=mask_obs.astype(np.uint8)
	dis_obs=cv2.subtract(DTM1,0,sp,mask_obs)
	return dis_obs
#convert depth map to binary image
def threshold(DTM,distance):
	DTM=255 *np.logical_and(DTM>distance,DTM<=distance+1.5)
	DTM = DTM.astype(np.uint8)
	return DTM
#Finds contours in a binary image or find area of obstacle in a binary image
def findContours(DTM,obstacle):
	(con,_) = cv2.findContours(DTM,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	con=sorted(con,key=cv2.contourArea,reverse=True)[:obstacle]
	return con

def sort_point(con1,obstacle):
	point_obstacle=np.zeros((obstacle,4))
	point1=np.zeros((obstacle,4))
	g=np.zeros(obstacle)
	i=0
	
	for jj in con1:
		pp=cv2.boundingRect(jj)
	#	print pp
		point1[i,0:5]=pp
		i=i+1
	#print point1
	g=point1[0:obstacle+1,0].copy()
	#print g
	g.sort()
	#print g
	for f in range(obstacle):
		for k in range(obstacle):
			if g[f]==point1[k,0]:
				point_obstacle[f,0:obstacle+1]=point1[k,0:obstacle+1]
	#print point_obstacle
	return point_obstacle
		
#find size of obstacle
def findsize(new_pixelW,distance):
	S2=new_pixelW*cm_baseW/pixel_baseW*((distance)/2)
	new_HW=int(S2)
	return new_HW
#find region of obstacle	
def sel_region(p):
	d1=p[0]
	d3=p[0]+p[2]
	if  d1<160 :
		a='1'
	elif  d1>=160 and d1<320:
		a='2'
	elif d1>=320 and d1<480:
		a='3'
	else:
		a='4'
	if  d3<160 :
		a=a+'1'
	elif  d3>=160 and d3<320:
		a=a+'2'
	elif d3>=320 and d3<480:
		a=a+'3'
	else:
		a=a+'4'
	return a

def sendToClient(frame):
	rgb_array=np.asarray(frame[:,:])
	result, imgencode = cv2.imencode('.jpg', rgb_array)
	data = np.array(imgencode)
	stringData = data.tostring()
	conn.send( str(len(stringData)).ljust(16))
	conn.send( stringData )
	
if __name__ == "__main__":
	
	#acc_ser=raw_input("Do you send images to client : [y/n] :>> ")
	acc_ser="n"
	if acc_ser=="y":
		sckt=socket.socket()
		sckt.bind(('127.0.0.1',8004))
		sckt.listen(1)
		conn,addr=sckt.accept()
		print " conntect to "+ str(addr)
	else:
		print "don't send images to client "
	
	ser=serial.Serial('/dev/ttyUSB0',38400,timeout=.001)	#access to the serial port
	ser.close()									#close serial port 
	ser.open()									#open serial port
	if ser.isOpen():
		print 'serial port is open'
		ser.write('**START**')
		font=cv.InitFont( cv.CV_FONT_HERSHEY_SIMPLEX, 1, 1,0,3,8)
		obstacle=3
		point=np.array([0,0,0])
		cm_baseW=55
		pixel_baseW=132
		sp=np.zeros((480,640))
		ref_gro=np.zeros((480,640))
		ref_gro=ref_ground()
		small_img=cv.CreateImage((320,240),8,3)
		gray_img=cv.CreateImage((320,240),8,1)
		sel_color='**rgb**'
		sel_flight='**ground**'
		print 'dddd' + sel_flight
		while(True):
			line=ser.readline()
			if line:
				if line=='**gray**' or line=='**rgb**':
					sel_color=line
					
				elif line=='**flight**' or line=='**ground**':
					sel_flight=line
					
			depth=getDepthKinect()
			DTM_8=depth.astype(np.uint8)
			rgb=getRGBKinect()
			DTM1=convDepthToMeter(depth)
			if sel_flight=='**ground**':
				dis_obs=remove_ground(DTM1)
			elif sel_flight=='**flight**':
				dis_obs=DTM1.copy()
				
			dis_obs[dis_obs<=0]=np.inf
			dis_min= dis_obs.min()
			obs_detect=threshold(dis_obs,dis_min)
			con=findContours(obs_detect,obstacle)
			p=np.array([0,0,0,0])
			point_dis_copy=np.array([0,0,0,0])
			data_distance=['nan','nan','nan','nan']
			data=''
			if not(con):
				ser.write('**zero**')
			else:
				
				point_dis=sort_point(con,obstacle)
				#print point_dis
				i=0
				for j in con:
					point=cv2.boundingRect(j)
					obs_rect=dis_obs[point[1]:(point[1]+point[3]),point[0]:(point[0]+point[2])].copy()
					if obs_rect.shape[0]!=0 and obs_rect.shape[1]!=0:
						dismin_rec=obs_rect.min()
					sizeObject=findsize(point[3],dismin_rec)
					data_distance[i]=str(findsize((point_dis[i,0]-(point_dis_copy[0]+point_dis_copy[2])),dismin_rec))
					point_dis_copy=np.copy(point_dis[i,0:4])
					cv.Rectangle(rgb,(point[0],point[1]),(point[0]+point[2],point[1]+point[3]),(0,0,255),thickness=1, lineType=8, shift=0)
					region=(sel_region(p))
					data=data+str(i+1) + '/' + str(int(dismin_rec*100)) + '/' + str(sizeObject) + '/' + str(region)+'**'
					cv.PutText(rgb,str(sizeObject),(point[0]+point[2]/3,point[1]+point[3]/3),font, cv.Scalar(0,0,255))
					cv.PutText(rgb,str(round(dismin_rec,2)),(point[0]+point[2]/2,point[1]+point[3]/2),font, cv.Scalar(0,255,0))
					i=i+1
			data_distance[i]=str(findsize((640-(point_dis_copy[0]+point_dis_copy[2])),dismin_rec))
			data_distance_total=data_distance[0]+'/'+data_distance[1]+'/'+data_distance[2]+'/'+data_distance[3]
			
			ser.write('##'+data+'#*'+data_distance_total+'*#**')
			#print '#*'+data_distance_total+'*#**'
			cv.Resize(rgb, small_img, cv.CV_INTER_CUBIC)
			cv.ShowImage('rgb_server',small_img)
			if acc_ser=='y':
				if sel_color =='**gray**':
					cv.CvtColor(small_img,gray_img,cv2.COLOR_RGB2GRAY)
					sendToClient(gray_img)
				elif sel_color=='**rgb**':
					sendToClient(small_img)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break 
	else:
		print 'serial port is not open.try again'
	ser.close()
	if acc_ser=='y':
		sckt.close()
	cv2.destroyAllWindows()
