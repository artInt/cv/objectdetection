#!/usr/bin/python

from subprocess import Popen,PIPE
import os
import time


def keypress(sequence):
    p = Popen(['xte'], stdin=PIPE)
    p.communicate(input=sequence)


shift_a_sequence = '''keydown Alt_L
key F4
keyup Alt_L
'''

os.system("gnome-terminal -e 'bash -c \"sudo freenect-glview; exec bash\" ' ")
time.sleep(5)

keypress(shift_a_sequence)
keypress(shift_a_sequence)

os.system("gnome-terminal -e 'bash -c \"sudo python ObsDetect.py; exec bash\" ' ")
