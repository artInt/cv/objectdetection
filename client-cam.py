import socket 
import cv2
import numpy

def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

sckt=socket.socket()
sckt.connect(('127.0.0.1',8004))

while 1:
    length = recvall(sckt,16)
    stringData = recvall(sckt, int(length))
    data = numpy.fromstring(stringData, dtype='uint8')
    decimg=cv2.imdecode(data,1)
    cv2.imshow('rgb_Client',decimg)
    cv2.waitKey(10)
sckt.close()
cv2.destroyAllWindows() 